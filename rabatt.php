<?php

require_once "./config.inc.php";

// Prüfen ob eine Rabatt-Art angegeben wurde
if(!isset($_POST["rabatt"]) || empty($_POST["rabatt"])) {
    die(json_encode(array('errCode' => 1, 'errText' => 'No discount type has been transferred', 'code' => '')));
}

// Verbindung zur Datenbank aufbauen
mysql_connect($hostname, $username, $password) or die("Error connecting to database");
mysql_select_db($database) or die("Error selecting database");

// UTF-8 nutzen
mysql_query("SET NAMES 'utf8'");

// Rabatt-Art auslesen
$rabatt_art = mysql_escape_string($_POST["rabatt"]);

// Rabatt-Code auswählen der zur Art passt, noch nicht verwendet wurde und 
// dessen Reservierung älter als 5 Minuten ist
$sql = "SELECT * FROM RabattCode r WHERE (r.typ = '$rabatt_art' AND r.genutzt IS NULL AND (r.reserviert IS NULL OR r.reserviert < (DATE_SUB(CURDATE(), INTERVAL 5 MINUTE)))) ORDER BY id LIMIT 1";
$res = mysql_query($sql);

if(!$res) {
    die(json_encode(array('errCode' => 2, 'errText' => mysql_error(), 'code' => '')));
}

// Prüfen ob ein Code zurückgegeben wurde
if(mysql_num_rows($res) !== 1) {
    die(json_encode(array('errCode' => 3, 'errText' => 'There are no more codes available!', 'code' => '')));
}

// Ergebniszeile holen
$row = mysql_fetch_assoc($res);

// Code als reserviert markieren
$res_id = $row['id'];
$res_dt = date("Y-m-d H:i:s");

$sql = "UPDATE RabattCode SET reserviert='$res_dt' WHERE id=$res_id";
$res = mysql_query($sql);

// Code zurückgeben
die(json_encode(array('errCode' => 0, 'errText' => mysql_error(), 'code' => $row['code'])));