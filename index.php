<?php

    require_once "./config.inc.php";
    require_once '/popup.php';

    // GET-Parameter abrufen
    $clid 	    = isset($_GET['clid'])      ? "0" . $_GET['clid']   : null;
    $uid 	    = isset($_GET['uid'])       ? $_GET['uid']          : null;
    $tp1 	    = isset($_GET['tp1'])       ? $_GET['tp1']          : null;
    $datum 	    = date('Y-m-d');
    $tp1 	    = $datum . " " . $tp1;
    $project    = isset($_GET['cnumb'])     ? $_GET['cnumb']        : null;
    $ipnumb     = isset($_GET['ipnumb'])    ? $_GET['ipnumb']       : null;
    $agent      = isset($_GET['agent'])     ? $_GET['agent']        : null;
    $chanin     = isset($_GET['chanin'])    ? $_GET['chanin']       : null;
    $created    = date("Y-m-d H:i:s");
    
    // Verbindung zur Datenbank aufbauen
    mysql_connect($hostname, $username, $password) or die("Error connecting to database");
    mysql_select_db($database) or die("Error selecting database");

    // UTF-8 nutzen
    mysql_query("SET NAMES 'utf8'");
    
    // Alle Kategorien und die dazugehörigen Anfragen laden
    $sql = "SELECT a.*, k.id as 'kategorie_id', k.bezeichnung AS 'kategorie', k.felder AS 'felder' FROM Anrufgrund a LEFT JOIN Kategorie k ON k.id = a.kategorie ORDER BY k.order_number, a.id";
    
    $res = mysql_query($sql);
    
    if(!$res) {
        die("Error collecting data");
    }
    
    $kategorien = array();
    $jsonItems  = array();
    $jsonFelder = array();       
    
    // Kategorien wird ein Array, welches als Index den Kategorienamen hat und 
    // als Wert ein Array mit zusätzlicher Feldern und den Anrufgründen, 
    // ebenfalls als Array
    while(($row = mysql_fetch_assoc($res)) !== false)
    {
        $kLabel = $row['kategorie'];
        $felder = json_decode($row['felder'], true);
        
        $item   = array('id'            => $row['id'],
                        'bezeichnung'   => $row['bezeichnung'],
                        'standard_text' => $row['standard_text'],
                        'anweisung'     => nl2br($row['anweisung']),
                        'felder'        => $felder,
                        'show_code'     => $row['show_code']);
        
        $jsonItems[$row['id']]    = $item;
        
        if(!empty($felder)) {
            foreach($felder as $feld) {
                if(!in_array($feld, $jsonFelder)) {
                    $jsonFelder[]   = $feld;
                }
            }
        }
        
        if(!isset($kategorien[$kLabel]))
        {
            $items = array($item);
            $kategorien[$kLabel] = array('id' => $row['kategorie_id'], 
                'items' => $items);
        }
        else
        {
            $kategorien[$kLabel]['items'][] = $item;
        }
    }
    
    // Ticket erstellen und in Datenbank eintragen
    $sql = "INSERT INTO Ticket (created, anummer, uniqueid, tp1, sip, chanin, ipnumb, project) VALUES('$created', '$clid', '$uid', '$tp1', '$agent', '$chanin', '$ipnumb', '$project')";
    $res = mysql_query($sql);
    
    // ID des erstellten Tickets auslesen
    $tid = mysql_insert_id();

    // Popup Daten auslesen
    //$popup_id   = 101;
    //$popups     = getpopups($agent, $popup_id);

    $popup_oben  = ""; //utf8_encode($popups['popup_oben']);
    $popup_unten = ""; //utf8_encode($popups['popup_unten']);
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>GutMarkiert Ticketaufnahme</title>
        <meta charset="utf-8" />

        <!--<link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />-->
        <link type="text/css" rel="stylesheet" href="assets/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="assets/bubble.css" />

        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        
        <script src="assets/bootbox.min.js"></script>
        <script src="assets/popper.js"></script>
        
        <script>
        
        $(document).ready(function() {

            var formSubmit = false;
            var items = <?php echo json_encode($jsonItems); ?>;

            function createWarning(message) {
                $("#_divAlert").empty();
                $("#_divAlert").append('<div class="alert alert-danger" role="alert">' + message +'</div>');
            }

            $("input.bubbleText").click(function() {
                var id = $(this).val();
                var item = items[id];
                
                // BubbleText anzeigen
                if(item !== null && item.anweisung !== null) {
                    $("#bubbleText").html(item.anweisung);
                } else {
                    $("#bubbleText").html("");
                }
                
                // Gesprächsnotiz einfügen
                if(item !== null && item.standard_text !== null) {
                    $("#field_notiz").val(item.standard_text);
                } else {
                    $("#field_notiz").val("");
                }
                
                $("div.addField").hide();
                
                // Anhand der Kategorie die zusätzlichen Felder auslesen
                if(item.felder !== null) {
                    for(var i = 0; i < item.felder.length; i++) {
                        $("#addField_" + item.felder[i].name).show();
                    }
                }
                
                // Prüfen ob ein Gutschein-Code angezeigt werden soll
                if(parseInt(item.show_code) === 1) {
                    $("#div_rabatt").show();
                } else {
                    $("#div_rabatt").hide();
                }
            });
            
            $("#btnGetRabattCode").click(function() {
                
                var rabatt = $("input[name='rabatt']:checked").val();
                
                if(rabatt === undefined) {
                    alert("Wybierz rodzaj zniżki!");
                    return false;
                }
                
                $.ajax({
                    url: "rabatt.php",
                    type: "post",
                    data: { rabatt: rabatt },
                    success: function(response) {
                    
                        var data = $.parseJSON(response);
                        
                        switch(data.errCode) {
                            case 0:
                                $("#field_rabattcode").val(data.code);
                                break;
                            case 1:
                                $("#field_rabattcode").val("");
                                alert("Wybierz rodzaj zniżki!");
                                break;
                            case 2:
                                $("#field_rabattcode").val("");
                                alert(data.errText);
                                break;
                            case 3:
                                $("#field_rabattcode").val("");
                                alert("Niestety wszystkie kody są zużyte! Proszę poinformować klienta, że ​​otrzyma kod drogą e-mail. Wybierz przyczynę wywołania i poinformuj menedżera projektu o problemie!");
                                break;
                        }
                        
                    }
                });
            });
            
            $("input[name='field_anrufgrund']").change(function() {
                $("#field_rabattcode").val("");
            });
            
            $("#submit_form").submit(function() {
                
                var agent   = $.trim($("#field_agent").val());
                var anrufer = $.trim($("#field_name").val());
                var telefon = $.trim($("#field_telefon").val());
                var email   = $.trim($("#field_email").val());
                var notiz   = $.trim($("#field_notiz").val());
                
                var radio = $("input[name='field_anrufgrund']:checked").val();
                
                // Agentennamen prüfen
                if(agent === "") {
                    $("#field_agent").focus();
                    alert("Wprowadź swoją nazwę agenta!");
                    
                    return false;
                }
                
                // Agentennamen prüfen
                if(anrufer === "") {
                    $("#field_name").focus();
                    alert("Proszę podać nazwę dzwoniącego!");
                    
                    return false;
                }
                
                // Anrufgrund prüfen
                if(radio === undefined) {
                    alert("Wybierz przyczynę swojego połączenia!");
                    
                    return false;
                }
                
                // Anrufnotiz prüfen
                if(notiz === "") {
                    $("#field_notiz").focus();
                    alert("Wpisz wiadomość w polu poniżej!");
                    
                    return false;
                }
                
                // Zusatzfelder prüfen
                var zusatz = true;
                
                $("div.addField:visible").each(function(index, el) {
                    var label = $(el).children("label");
                    var field = $(el).children("input");
                    
                    var value = $.trim($(field[0]).val());
                    
                    if($(field[0]).data("pflicht") === 1 && value === "") {
                        $(field[0]).focus();
                        alert("Please fill in the field " + $(label[0]).text() + "!");
                        
                        zusatz = false;
                        return;
                    }
                    
                });
                
                if(zusatz === false) {
                    return false;
                }
                
                if($("#div_rabatt").is(':visible') && $("#field_rabattcode").val() === "") {
                    alert("Proszę wygenerować kod rabatowy!");
                    return false;
                }

                formSubmit = true;
                return true;
            });

            window.onbeforeunload = function() {

                if(formSubmit === false) {

                    createWarning("<strong>Ticket missing</strong> Proszę wziąć bilet za połączenie!");

                    return 'Proszę wziąć bilet za połączenie!';
                }
            };
        });
        
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    
                    <h2>Gutmarkiert, Dziendobry, rozmawia Pan/Pani z (Imie, Nazwisko). Jak moge Pani/Panu pomuc?</h2>

                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $popup_oben; ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12" id="_divAlert">
                            <!-- Alert message created at runtime -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-7">
                            <form role="form" method="post" id="submit_form" action="speichern.php">
                                <div class="form-group">
                                    <label for="field_agent">Imię i nazwisko agenta*</label>
                                    <input type="text" class="form-control" id="field_agent" name="field_agent" placeholder="Imię i nazwisko pracownika" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="field_name">Imię i nazwisko osoby dzwoniącej*</label>
                                    <input type="text" class="form-control" id="field_name" name="field_name" placeholder="Pełna nazwa osoby dzwoniącej" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="field_telefon">Numer oddzwaniania</label>
                                    <input type="text" class="form-control" id="field_telefon" name="field_telefon" placeholder="Możliwy numer oddzwaniania">
                                </div>

                                <div class="form-group">
                                    <label for="field_email">Adres e-mail</label>
                                    <input type="email" class="form-control" id="field_email" name="field_email" placeholder="Adres e-mail osoby dzwoniącej">
                                </div>

                                <div class="form-group">
                                    <label>Przyczyna wywołania*</label>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="panel-group" id="accordion">

                                            <?php
                                                foreach($kategorien as $label => $kategorie)
                                                {
                                                    // Accordion Item
                                                    echo "<div class=\"panel panel-default\">";

                                                    // Accordion Item Header
                                                    echo "<div class=\"panel-heading\">";
                                                    echo "<h4 class=\"panel-title\">";
                                                    echo "<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse_" . $kategorie['id'] . "\">";
                                                    echo $label;
                                                    echo "</a>";
                                                    echo "</h4>";
                                                    echo "</div>";

                                                    // Accordion Item Body
                                                    echo "<div id=\"collapse_" . $kategorie['id'] . "\" class=\"panel-collapse collapse\">";
                                                    echo "<div class=\"panel-body\">";

                                                    foreach($kategorie['items'] as $item)
                                                    {
                                                        echo "<div class=\"checkbox\">";
                                                        echo "<label>";
                                                        echo "<input class=\"bubbleText\" type=\"radio\" name=\"field_anrufgrund\" value=\"" . $item['id'] . "\"> " . $item['bezeichnung'];
                                                        echo "</label>";
                                                        echo "</div>";
                                                    }

                                                    echo "</div>";
                                                    echo "</div>";

                                                    // Accordion Item End
                                                    echo "</div>";
                                                }
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field_notiz">Notatka połączeń*</label>
                                    <textarea name="field_notiz" id="field_notiz" class="form-control" rows="6" required="required"></textarea>
                                </div>

                                <?php
                                    foreach($jsonFelder as $feld)
                                    {
                                        echo "<div id=\"addField_" . $feld['name'] . "\" class=\"form-group addField\" style=\"display: none;\">";
                                        echo "<label for=\"" . $feld['name'] . "\">" . $feld['label'] . (isset($feld['data-pflicht']) && $feld['data-pflicht'] == '1' ? "*" : "") . "</label>";
                                        echo "<input class=\"form-control\" ";
                                        foreach($feld as $key => $value) {
                                            if($key != "label") {
                                                echo "$key=\"$value\"";
                                            }
                                        }
                                        echo "/>";
                                        echo "</div>";
                                    }
                                ?>

                                <div id="div_rabatt" style="display: none;">
                                    <h3>Utwórz kod rabatowy</h3>
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="rabatt" value="10%"> 10%
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="rabatt" value="20%"> 20%
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="rabatt" value="GAV">Darmowa dostawa
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" id="btnGetRabattCode" class="btn">Uzyskaj kod rabatowy</button>
                                    </div>

                                    <div class="form-group">
                                        <label for="field_email">Kod rabatowy</label>
                                        <input type="text" class="form-control" id="field_rabattcode" name="field_rabattcode" placeholder="Kliknij, aby uzyskać kod rabatowy..." readonly="readonly">
                                    </div>


                                </div>

                                <input type="hidden" name="tid" value="<?php echo $tid; ?>" />
                                <button type="submit" class="btn btn-default" name="submit">Prześlij dane</button>
                            </form>
                        </div>

                        <div class="col-md-5">
                            <blockquote class="oval-thought-border">
                                <p id="bubbleText"></p>
                            </blockquote>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <?php echo $popup_unten; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <h3>Dostęp do CRM</h3>
                    <p>
                        Username: zweiloewen<br/>
                        Password: 6StgGRiuT2
                    </p>
                    <iframe src="http://www.goedgemerkt.nl/l3b3l5" style="width: 100%; height: 800px" scrolling="auto">
                </div>
            </div>
        </div>
    </body>
</html>