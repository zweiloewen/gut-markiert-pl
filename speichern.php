<?php

$from_mail = "gut-markiert@support.2loewen.de";

if(isset($_POST['submit']))
{
    require_once "./config.inc.php";

    // Verbindung zur Datenbank aufbauen
    mysql_connect($hostname, $username, $password) or die("Error connecting to database");
    mysql_select_db($database) or die("Error selecting database");

    // UTF-8 nutzen
    mysql_query("SET NAMES 'utf8'");
    
    // POST-Parameter auf Array mappen
    $post_data = $_POST;
    
    // Werte vordefinieren
    $tid        = 0;
    $agent      = "";
    $anrufer    = "";
    $rueckruf   = "";
    $email      = "";
    $anrufgrund = "";
    $anrufnotiz = "";
    $sonstiges  = "";
    $rabattcode = "";
    $finished   = date("Y-m-d H:i:s");
    
    // POST-Daten auslesen
    if(isset($post_data['tid'])) {
        $tid = $post_data['tid'];
        unset($post_data['tid']);
    }
    
    if(isset($post_data['field_agent'])) {
        $agent = mysql_escape_string($post_data['field_agent']);
        unset($post_data['field_agent']);
    }
    
    if(isset($post_data['field_name'])) {
        $anrufer = mysql_escape_string($post_data['field_name']);
        unset($post_data['field_name']);
    }
    
    if(isset($post_data['field_telefon'])) {
        $rueckruf = mysql_escape_string($post_data['field_telefon']);
        unset($post_data['field_telefon']);
    }
    
    if(isset($post_data['field_email'])) {
        $email = mysql_escape_string($post_data['field_email']);
        unset($post_data['field_email']);
    }
    
    if(isset($post_data['field_anrufgrund'])) {
        $anrufgrund = mysql_escape_string($post_data['field_anrufgrund']);
        unset($post_data['field_anrufgrund']);
    }
    
    if(isset($post_data['field_notiz'])) {
        $anrufnotiz = mysql_escape_string($post_data['field_notiz']);
        unset($post_data['field_notiz']);
    }
    
    if(isset($post_data['field_rabattcode'])) {
        $rabattcode = mysql_escape_string($post_data['field_rabattcode']);
        unset($post_data['field_rabattcode']);
    }
    
    // Alle anderen POST-Parameter werden unter Sonstiges gespeichern
    $sonstiges = array();
    
    foreach($post_data as $key => $value) {
        if($key == 'submit' || $key == 'Rabatt') {
            continue;
        }
        
        $key = ucfirst(str_replace("field_", "", $key));
        
        
        $sonstiges[$key] = mysql_escape_string($value);
    }
    
    $json_sonstiges = json_encode($sonstiges);
    
    // Falls ein Rabattcode ausgewählt wurde, so muss dieser in der Datenbank 
    // als benutzt markiert werden
    $sql = "UPDATE RabattCode SET genutzt=CURDATE(), ticket_id=$tid WHERE code='$rabattcode'";
    $res = mysql_query($sql);
    
    // SQL-Query zusammenbauen und ausführen
    $sql = "UPDATE Ticket SET agent='$agent', anrufer='$anrufer', rueckruf='$rueckruf', email='$email', anrufgrund='$anrufgrund', anrufnotiz='$anrufnotiz', sonstiges='$json_sonstiges', rabattcode='$rabattcode', finished='$finished' WHERE id=$tid";
    $res = mysql_query($sql);
    
    if(!$res) {
        die(mysql_error());
    }
    
    // Anrufgrund und Ziel-Email auslesen
    $sql = "SELECT bezeichnung, ticket_mail FROM Anrufgrund WHERE id = $anrufgrund";
    $res = mysql_query($sql);
    
    if(!$res) {
        die(mysql_error());
    }
    
    $data = mysql_fetch_assoc($res);
    
    // Benötigte Parameter für E-Mail holen
    $datum 	= date("d.m.Y");
    $uhrzeit 	= date("H:i:s");
    $anrufgrund = $data['bezeichnung'];
    $zielmail   = $data['ticket_mail'];
    
    // Email mit Ticket versenden
    $subject = "Anruf bei Zwei Loewen";
    $message = "Anruf für Sie bei Zwei Löwen:\n\nAnruf erfolgte am $datum\n\nAnruf erfolgte um: $uhrzeit\n\nAnrufer: $anrufer\nRückrufnummer: $rueckruf\nE-Mail: $email\n\nAnrufgrund: $anrufgrund\nGesprächsnotiz: $anrufnotiz\n\n";

    // Zusatzfelder einbinden
    foreach($sonstiges as $feld => $wert)
    {
        if(!empty($feld) && !empty($wert)) {
            $message .= "$feld: $wert\n";
        }
    }
    
    mail($zielmail, $subject, $message, "Content-type: text/plain; charset=utf-8\r\nFrom: $from_mail");

    $subject = "GutMarkiert: " . $subject;
    $message = "Mitarbeiter bei ZL: $agent\n\n".$message;

    if($zielmail != 'burgardt@zweiloewen.de') {
        mail("callcenter@verwaltung.2loewen.de, matthias.schluse@zweiloewen.com", $subject, $message, "Content-type: text/plain; charset=utf-8\r\nFrom: $from_mail");
    }
}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>GutMarkiert Ticketaufnahme</title>
        <meta charset="utf-8" />

        <!--<link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />-->
        <link type="text/css" rel="stylesheet" href="assets/bootstrap.css" />
    </head>
    
    <body>
        <div class="container">
            <h2>Ticket saved</h2>
            <div class="alert alert-success" role="alert">
                Dziękuję Ci bardzo! Bilet został pomyślnie zapisany.
            </div>
        </div>
    </body>
</html>