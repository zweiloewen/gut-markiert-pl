var Popper = function(){};
 
Popper.prototype.confirm = function(msg, strCancel, strConfirm){
    var deferred = new $.Deferred();
    
    bootbox.confirm(msg, function(confirmed) {
        deferred[confirmed ? 'resolve' : 'reject']();
    });
    
    return deferred.promise();
};