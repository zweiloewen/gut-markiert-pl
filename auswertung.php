<?php
/**
 * User: Felix Göcking <felix.goecking@zweiloewen.com>
 * Date: 14.07.2015
 * Time: 09:43
 */

function curlRequest($url, $data) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/x-www-form-urlencoded',
        'Content-Length: '.strlen($data)
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    $result = curl_exec($ch);
    $info   = curl_getinfo($ch);

    curl_close($ch);

    return array(
        'status_code' => $info['http_code'],
        'body' => $result
    );
}

function getCallData($uniqueIds = array()) {

    $response = curlRequest('http://web5.2loewen.de/projects/sip/web/index.php/api/v1/tp/json', json_encode($uniqueIds));

    if($response['status_code'] == 200 && $response['body'] !== "false") {
        return $response['body'];
    }

    return false;
}

if(isset($_POST['dateFrom']) && isset($_POST['dateTo'])) {

    // Parameters
    $dateFrom   = $_POST['dateFrom'] . " 00:00:00";
    $dateTo     = $_POST['dateTo'] . " 23:59:59";

    // Database settings
    $hostname = "db1.2loewen.de";
    $username = "gutmarkiert";
    $password = "8OsAh8QAn2dI";
    $database = "gut_markiert";

    // Open the database connection to GutMarkiert
    $connection = new PDO("mysql:host=".$hostname.";dbname=".$database, $username, $password);

    if (!$connection) {
        die("Could not connect to database");
    }

    // Get all tickets from the time range
    $statement = $connection->prepare("SELECT * FROM Ticket t WHERE t.created >= ? AND t.created <= ?");
    $statement->bindParam(1, $dateFrom);
    $statement->bindParam(2, $dateTo);

    if(!$statement->execute()) {
        die("Could not execute query - " . $statement->errorCode() . " - " . print_r($statement->errorInfo()));
    }

    $uniqueIds = array();
    $tickets = array();

    while(false !== ($row = $statement->fetch(PDO::FETCH_ASSOC))) {
        $uniqueId = $row['uniqueid'];

        $row['TP1'] = '';
        $row['TP6'] = '';
        unset($row['anrufnotiz']);
        unset($row['timestamp']);

        if(!empty($uniqueId)) {
            $uniqueIds[] = $uniqueId;
            $tickets[$uniqueId] = $row;
        } else {
            $tickets[] = $row;
        }
    }

    // Get the call data for the uniqueids
    $result = getCallData($uniqueIds);

    if($result !== false) {

        $data = json_decode($result);

        // Read call data for each ticket
        foreach ($data as $tpData) {

            $uid = $tpData->AUIQ;

            $tickets[$uid]['TP1'] = $tpData->TP1;
            $tickets[$uid]['TP6'] = $tpData->TP6;
        }
    }

    // Create csv file
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=Export_' . date("Y-m-d H:i:s") . '.csv');

    $output = fopen('php://output', 'w');

    fputcsv($output, array(
        'Ticket-ID',
        'Ticket erstellt',
        'Ticket abgeschlossen',
        'Warnung gesendet',
        'Anummer',
        'UniqueID',
        'TP1',
        'SIP',
        'Chanin',
        'IP',
        'Projekt',
        'Agentenname',
        'Anrufer',
        'Rückruf',
        'E-Mail',
        'Anrufgrund',
        'Sonstiges',
        'Rabattcode',
        'Call-Start',
        'Call-Ende'));

    foreach($tickets as $ticket) {
        fputcsv($output, $ticket);
    }

    exit;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Gutmarkiert Auswertung</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="auswertung.php">
                <div class="form-group">
                    <label for="dateFrom">Startdatum</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type="text" class="form-control" id="dateFrom" name="dateFrom">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateTo">Enddatum</label>
                    <div class='input-group date' id='datetimepicker1'>
                        <input type="text" class="form-control" id="dateTo" name="dateTo">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <button type="submit" class="btn btn-default">Auswertung holen</button>
            </form>
        </div>

        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/moment/min/moment-with-locales.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script>
            $(document).ready(function() {
                $("#dateFrom").datetimepicker({
                    locale: 'de',
                    format: 'YYYY-MM-DD'
                });
                $("#dateTo").datetimepicker({
                    locale: 'de',
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
    </body>
</html>